// This makes VSCode check types as if you are using TypeScript
//@ts-check
//import { useState, useEffect } from 'react'
//import ErrorNotification from './ErrorNotification'
//import Construct from './Construct'
import About from './About'
import Users from './Users'
import './App.css'
import { BrowserRouter,Route, Routes } from 'react-router-dom'
// All your environment variables in vite are in this object


function App() {


    return (
  
            <BrowserRouter>
                <Routes>
                    <Route path="/*" element={<Users />} />
                    <Route path="/about" element={<About />} />
                </Routes>
            </BrowserRouter>
            
  
    )
}

export default App
