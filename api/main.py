from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os

from db import UserRepository, UserIn, UserOut

app = FastAPI()

allowed_origins = [ "http://localhost:5173", "https://hackreactordemo.gitlab.io"]

app.add_middleware(
    CORSMiddleware,
    allow_origins= ["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# create an endpoint to create a user in the database
@app.post("/api/users")
def create_user(user: UserIn):
    return UserRepository().create(user)

# create an endpoint to get a user from the database
@app.get("/api/users/{email}")
def get_user(email: str):
    return UserRepository().get(email)

# create an endpoint to get all users from the database
@app.get("/api/users")
def get_all_users():
    return UserRepository().get_all_users()

# delete user from the database
@app.delete("/api/users/{id}")
def delete_user(id: int):
    return UserRepository().delete(id)

@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    }


