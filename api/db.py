import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))


class UserIn(BaseModel):
    email: str

class UserOut(BaseModel):
    id: int
    email: str


class UserRepository:
    def get_all_users(self):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        SELECT *
                        FROM users
                    """
                )
                return [UserOut(id=record[0], email=record[1]) for record in result]

    def get(self, email: str) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        SELECT *
                        FROM users
                        WHERE email = %s
                    """,
                    [email]
                )
                record = result.fetchone()
                if record is None:
                    return None
                return UserOut(
                    id=record[0],
                    email=record[1],
                )
    # delete a user from the database
    def delete(self, id: int) -> UserOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        DELETE FROM users
                        WHERE id = %s
                        RETURNING *;
                    """,
                    [id]
                )
                record = result.fetchone()
                return UserOut(
                    id=record[0],
                    email=record[1],
                )


    def create(self, info: UserIn) -> UserOut:
        print("we made it into the create method!!!!!!!!!!")
        with pool.connection() as conn:
            print("we made it through the pool connection")
            with conn.cursor() as db:
                print("we made it into the cursor")
                result = db.execute(
                    """
                        INSERT INTO users
                        (email)
                        VALUES
                        (%s)
                        RETURNING *;
                    """,
                    [
                     info.email
                     ]
                )

                record = result.fetchone()
                print("here is the record:", record)
                return UserOut(
                    id=record[0],
                    email=record[1],
                )
